# image

#### 介绍
图片缩放

3.  xxxx

#### 使用说明
use Lhp\Image\Gd\Driver;

$img = (new Driver())->init($image);

$height = $img->getWidth();

$width = $img->getHeight();

$wHRate = $width / $height;

$basename = basename($image);

$medium = $img->resize(400, 400 * $wHRate);

$medium->save(str_replace($basename, 'medium-'.$basename, $image), 90);

$small = $img->resize(250, 250 * $wHRate);

$small->save(str_replace($basename, 'small-'.$basename, $image), 90);


