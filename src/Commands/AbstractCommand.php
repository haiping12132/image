<?php

namespace Lhp\Image\Commands;

use Lhp\Image\Commands\Argument;

abstract class AbstractCommand
{
    /**
     * Arguments of command
     *
     * @var array
     */
    public array $arguments;

    /**
     * Output of command
     *
     * @var mixed
     */
    protected $output;

    /**
     * Executes current command on given image
     *
     * @param  Image $image
     * @return mixed
     */
    abstract public function execute($image): mixed;

    /**
     * Creates new command instance
     *
     * @param array $arguments
     */
    public function __construct($arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * Creates new argument instance from given argument key
     *
     * @param  int $key
     * @return Argument
     */
    public function argument(int $key): Argument
    {
        return new Argument($this, $key);
    }

    /**
     * Returns output data of current command
     *
     * @return mixed
     */
    public function getOutput(): mixed
    {
        return $this->output ? $this->output : null;
    }

    /**
     * Determines if current instance has output data
     *
     * @return boolean
     */
    public function hasOutput(): bool
    {
        return ! is_null($this->output);
    }

    /**
     * Sets output data of current command
     *
     * @param mixed $value
     */
    public function setOutput(mixed $value): void
    {
        $this->output = $value;
    }
}
