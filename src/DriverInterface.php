<?php

namespace Lhp\Image;

Interface DriverInterface
{

    /**
     * Creates new image instance
     *
     * @param  int     $width
     * @param  int     $height
     * @param  string  $background
     * @return Image
     */
    public function newImage(int $width, int $height, int $background): Image;

    /**
     * Returns clone of given core
     *
     * @return mixed
     */
    public function cloneCore(mixed $core): mixed;


    /**
     * Encodes given image
     *
     * @param  Image   $image
     * @param  string  $format
     * @param  int     $quality
     * @return Image
     */
    public function encode(Image $image, string $format, int $quality): Image;
}
