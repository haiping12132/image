<?php

namespace Lhp\Image;

Interface DecoderInterface
{
    /**
     * Initiates new image from path in filesystem
     *
     * @param  string $path
     * @return \Lhp\Image\Image
     */
    public function initFromPath(string $path): Image;

    /**
     * Initiates new image from binary data
     *
     * @param  string $data
     * @return \Lhp\Image\Image
     */
    public function initFromBinary(string $data): Image;

    /**
     * Initiates new image from GD resource
     *
     * @param  Resource $resource
     * @return \Lhp\Image\Image
     */
    public function initFromGdResource(mixed $resource): Image;

    /**
     * Initiates new image from Imagick object
     *
     * @param \Imagick $object
     * @return \Lhp\Image\Image
     */
    public function initFromImagick(\Imagick $object): Image;

    /**
     * Init from given stream
     *
     * @param resource $stream
     * @return \Lhp\Image\Image
     */
    public function initFromStream(mixed $stream): Image;


    /**
     * Determines if current source data is GD resource
     *
     * @return boolean
     */
    public function isGdResource(): bool;

    /**
     * Determines if current source data is Imagick object
     *
     * @return boolean
     */
    public function isImagick(): bool;


    /**
     * Determines if current data is SplFileInfo object
     *
     * @return boolean
     */
    public function isSplFileInfo(): bool;


    /**
     * Determines if current source data is file path
     *
     * @return boolean
     */
    public function isFilePath(): bool;

    /**
     * Determines if current source data is url
     *
     * @return boolean
     */
    public function isUrl(): bool;

    /**
     * Determines if current source data is a stream resource
     *
     * @return boolean
     */
    public function isStream(): bool;

    /**
     * Determines if current source data is binary data
     *
     * @return boolean
     */
    public function isBinary(): bool;

    /**
     * Determines if current source data is data-url
     *
     * @return boolean
     */
    public function isDataUrl(): bool;

    /**
     * Determines if current source data is base64 encoded
     *
     * @return boolean
     */
    public function isBase64(): bool;

    /**
     * Initiates new Image from Lhp\Image\Image
     *
     * @param  Image $object
     * @return \Lhp\Image\Image
     */
    public function initFromImage(Image $object): Image;
}
