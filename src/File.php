<?php

namespace Lhp\Image;

class File
{
    /**
     * Mime type
     *
     * @var string
     */
    public string $mime;

    /**
     * Name of directory path
     *
     * @var string
     */
    public string $dirname;

    /**
     * Basename of current file
     *
     * @var string
     */
    public string $basename;

    /**
     * File extension of current file
     *
     * @var string
     */
    public string $extension;

    /**
     * File name of current file
     *
     * @var string
     */
    public string $filename;

    /**
     * Sets all instance properties from given path
     *
     * @param string $path
     */
    public function setFileInfoFromPath(string $path): static
    {
        $info = pathinfo($path);
        $this->dirname = array_key_exists('dirname', $info) ? $info['dirname'] : null;
        $this->basename = array_key_exists('basename', $info) ? $info['basename'] : null;
        $this->extension = array_key_exists('extension', $info) ? $info['extension'] : null;
        $this->filename = array_key_exists('filename', $info) ? $info['filename'] : null;

        if (file_exists($path) && is_file($path)) {
            $this->mime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
        }

        return $this;
    }

     /**
      * Get file size
      * 
      * @return mixed
      */
    public function filesize(): mixed
    {
        $path = $this->basePath();

        if (file_exists($path) && is_file($path)) {
            return filesize($path);
        }
        
        return false;
    }

    /**
     * Get fully qualified path
     *
     * @return string
     */
    public function basePath(): ?string
    {
        if ($this->dirname && $this->basename) {
            return ($this->dirname .'/'. $this->basename);
        }

        return null;
    }

}
