<?php

namespace Lhp\Image;

use Lhp\Image\Commands\AbstractCommand;
use Lhp\Image\Exception\NotSupportedException;

abstract class DriverAbstract implements DriverInterface
{
    /**
     * Decoder instance to init images from
     */
    public DecoderInterface $decoder;

    /**
     * Image encoder instance
     */
    public EncoderInterface $encoder;

    /**
     * Checks if core module installation is available
     *
     * @return boolean
     */
    abstract protected function coreAvailable();

    /**
     * Returns clone of given core
     *
     * @return mixed
     */
    public function cloneCore(mixed $core): mixed
    {
        return clone $core;
    }

    /**
     * Initiates new image from given input
     *
     * @param  mixed $data
     * @return Image
     */
    public function init(mixed $data): Image
    {
        return $this->decoder->init($data);
    }

    /**
     * Encodes given image
     *
     * @param  Image   $image
     * @param  string  $format
     * @param  int     $quality
     * @return Image
     */
    public function encode(Image $image, string $format, int $quality): Image
    {
        return $this->encoder->process($image, $format, $quality);
    }

    /**
     * Returns name of current driver instance
     *
     * @return string
     */
    public function getDriverName(): string
    {
        $reflect = new \ReflectionClass($this);
        $namespace = $reflect->getNamespaceName();
        return substr(strrchr($namespace, "\\"), 1);
    }

    /**
     * Returns classname of given command name
     *
     * @param  string $name
     * @return string
     */
    private function getCommandClassName($name): string
    {
        if (extension_loaded('mbstring')) {
            $name = mb_strtoupper(mb_substr($name, 0, 1)) . mb_substr($name, 1);
        } else {
            $name = strtoupper(substr($name, 0, 1)) . substr($name, 1);
        }

        $drivername = $this->getDriverName();
        $classnameLocal = sprintf('\Lhp\Image\%s\Commands\%sCommand', $drivername, ucfirst($name));
        $classnameGlobal = sprintf('\Lhp\Image\Commands\%sCommand', ucfirst($name));

        if (class_exists($classnameLocal)) {
            return $classnameLocal;
        } elseif (class_exists($classnameGlobal)) {
            return $classnameGlobal;
        }

        throw new NotSupportedException(
            "Command ({$name}) is not available for driver ({$drivername})."
        );
    }

    /**
     * Executes named command on given image
     *
     * @param  Image  $image
     * @param  string $name
     * @param  array $arguments
     * @return AbstractCommand
     */
    public function executeCommand($image, $name, $arguments): AbstractCommand
    {
        $commandName = $this->getCommandClassName($name);
        $command = new $commandName($arguments);
        $command->execute($image);
        return $command;
    }
}
