<?php

namespace Lhp\Image\Gd\Commands;


use Lhp\Image\Commands\AbstractCommand;
use Lhp\Image\Image;
use Lhp\Image\Size;

class GetSizeCommand extends AbstractCommand
{
    /**
     * Reads size of given image instance in pixels
     *
     * @param  Image $image
     * @return boolean
     */
    public function execute($image): bool
    {
        $this->setOutput(new Size(
            imagesx($image->getCore()),
            imagesy($image->getCore())
        ));

        return true;
    }
}
