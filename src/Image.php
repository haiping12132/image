<?php

namespace Lhp\Image;

use Lhp\Image\Exception\NotSupportedException;
use Lhp\Image\Exception\NotWritableException;
use Lhp\Image\Exception\RuntimeException;

/**
 * @method \Lhp\Image\Image resize(int $width = null, int $height = null, \Closure $callback = null)
 */
class Image extends File
{
    /**
     * Instance of current image driver
     */
    protected DriverInterface $driver;

    /**
     * Image resource/object of current image processor
     *
     * @var mixed
     */
    protected mixed $core;

    /**
     * Array of Image resource backups of current image processor
     */
    protected array $backups = [];

    /**
     * Last image encoding result
     *
     * @var string
     */
    public string $encoded = '';

    /**
     * Creates a new Image instance
     *
     * @param DriverAbstract $driver
     * @param mixed  $core
     */
    public function __construct(DriverInterface $driver = null, mixed $core = null)
    {
        $this->driver = $driver;
        $this->core = $core;
    }

    /**
     * Magic method to catch all image calls
     * usually any AbstractCommand
     *
     * @param  string $name
     * @param  Array  $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $command = $this->driver->executeCommand($this, $name, $arguments);
        return $command->hasOutput() ? $command->getOutput() : $this;
    }

    /**
     * Starts encoding of current image
     *
     * @param  string  $format
     * @param  int     $quality
     * @return \Lhp\Image\Image
     */
    public function encode(string $format = null, int $quality = 90)
    {
        return $this->driver->encode($this, $format, $quality);
    }

    /**
     * Saves encoded image in filesystem
     *
     * @param  string  $path
     * @param  int     $quality
     * @param  string  $format
     * @return Image
     */
    public function save(string $path = null, int $quality = null, string $format = null): static
    {
        $path = is_null($path) ? $this->basePath() : $path;

        if (is_null($path)) {
            throw new NotWritableException(
                "Can't write to undefined path."
            );
        }

        if ($format === null) {
            $format = pathinfo($path, PATHINFO_EXTENSION);
        }

        $data = $this->encode($format, $quality);
        $saved = @file_put_contents($path, $data);

        if ($saved === false) {
            throw new NotWritableException(
                "Can't write image data to path ({$path})"
            );
        }

        // set new file info
        $this->setFileInfoFromPath($path);
        return $this;
    }

    /**
     * Runs a given filter on current image
     *
     * @param  FiltersFilterInterface $filter
     * @return Image
     */
    public function filter(Filters\FilterInterface $filter): Image
    {
        return $filter->applyFilter($this);
    }

    /**
     * Returns current image driver
     *
     * @return DriverInterface
     */
    public function getDriver() : DriverInterface
    {
        return $this->driver;
    }

    /**
     * Sets current image driver
     * @param DriverAbstract $driver
     */
    public function setDriver(DriverAbstract $driver)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * set source
     * @param mixed $source
     * @return $this
     */
    public function setSource(mixed $source): static
    {
        if ( ! $this->driver) {
            $this->createDriver();
        }
        $this->driver->init($source);
        return $this;
    }

    /**
     * Returns current image resource/obj
     *
     * @return mixed
     */
    public function getCore()
    {
        return $this->core;
    }

    /**
     * Sets current image resource
     *
     * @param mixed $core
     */
    public function setCore($core)
    {
        $this->core = $core;

        return $this;
    }

    /**
     * Returns current image backup
     *
     * @param string $name
     * @return mixed
     */
    public function getBackup($name = null)
    {
        $name = is_null($name) ? 'default' : $name;

        if ( ! $this->backupExists($name)) {
            throw new RuntimeException(
                "Backup with name ({$name}) not available. Call backup() before reset()."
            );
        }

        return $this->backups[$name];
    }

    /**
     * Returns all backups attached to image
     *
     * @return array
     */
    public function getBackups(): array
    {
        return $this->backups;
    }

    /**
     * Sets current image backup
     *
     * @param mixed  $resource
     * @param string $name
     * @return self
     */
    public function setBackup(mixed $resource, string $name = null): static
    {
        $name = is_null($name) ? 'default' : $name;
        $this->backups[$name] = $resource;
        return $this;
    }

    /**
     * Checks if named backup exists
     *
     * @param  string $name
     * @return bool
     */
    private function backupExists(string $name): bool
    {
        return array_key_exists($name, $this->backups);
    }

    /**
     * Checks if current image is already encoded
     *
     * @return boolean
     */
    public function isEncoded(): bool
    {
        return ! empty($this->encoded);
    }

    /**
     * Returns encoded image data of current image
     *
     * @return string
     */
    public function getEncoded(): EncoderInterface
    {
        return $this->encoded;
    }

    /**
     * Sets encoded image buffer
     *
     * @param string $value
     */
    public function setEncoded($value): static
    {
        $this->encoded = $value;

        return $this;
    }

    /**
     * Calculates current image width
     *
     * @return int
     */
    public function getWidth(): int
    {
        return $this->getSize()->width;
    }

    /**
     * Alias of getWidth()
     *
     * @return int
     */
    public function width(): int
    {
        return $this->getWidth();
    }

    /**
     * Calculates current image height
     *
     * @return int
     */
    public function getHeight(): int
    {
        return $this->getSize()->height;
    }

    /**
     * Alias of getHeight
     *
     * @return int
     */
    public function height(): int
    {
        return $this->getHeight();
    }

    /**
     * Reads mime type
     *
     * @return string
     */
    public function mime(): string
    {
        return $this->mime;
    }

    /**
     * Returns encoded image data in string conversion
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->encoded;
    }

    /**
     * Cloning an image
     */
    public function __clone(): void
    {
        $this->core = $this->driver->cloneCore($this->core);
    }
}
