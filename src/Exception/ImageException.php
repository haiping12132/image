<?php

namespace Lhp\Image\Exception;

class ImageException extends \RuntimeException
{
    # nothing to override
}
