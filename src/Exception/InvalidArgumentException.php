<?php

namespace Lhp\Image\Exception;

class InvalidArgumentException extends ImageException
{
    # nothing to override
}
