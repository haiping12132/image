<?php

namespace Lhp\Image\Exception;

class NotWritableException extends ImageException
{
    # nothing to override
}
