<?php

namespace Lhp\Image\Exception;

class RuntimeException extends ImageException
{
    # nothing to override
}
