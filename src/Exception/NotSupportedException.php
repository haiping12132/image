<?php

namespace Lhp\Image\Exception;

class NotSupportedException extends ImageException
{
    # nothing to override
}
