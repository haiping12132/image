<?php

namespace Lhp\Image\Exception;

class NotFoundException extends ImageException
{
    # nothing to override
}
