<?php

namespace Lhp\Image\Exception;

class NotReadableException extends ImageException
{
    # nothing to override
}
