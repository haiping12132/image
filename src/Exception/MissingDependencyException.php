<?php

namespace Lhp\Image\Exception;

class MissingDependencyException extends ImageException
{
    # nothing to override
}
